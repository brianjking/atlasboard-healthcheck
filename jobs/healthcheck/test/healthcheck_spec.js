var helper = require('./helper.js');
var healthcheck = helper.require('healthcheck.js');

var appName = 'The Remote App';
var hostname = 'foo.com';
var url = 'https://' + hostname + '/healthcheck';

var config, job_callback, dependencies;

var sharedSpecs = {

  healthcheckFailure: function(failure) {

    beforeEach(function(done) {
      config = {url: url};
      done();
    });

    it('does not pass an error parameter', function(done) {
      healthcheck(config, dependencies, function(error, data) {
        expect(error).to.be.null;
      });
      done();
    });

    it('puts the error in .failure', function(done) {
      healthcheck(config, dependencies, function(error, data) {
        expect(data).to.have.property('failure', failure);
      });
      done();
    });

    it('is not healthy', function(done) {
      healthcheck(config, dependencies, function(error, data) {
        expect(data).to.have.property('isHealthy', false);
      });
      done();
    });

    it('uses hostname in widgetTitle', function(done) {
      healthcheck(config, dependencies, function(error, data) {
        expect(data).to.have.property('widgetTitle', hostname);
      });
      done();
    });

  },

  healthcheckAnything: function() {

    it('uses the configured url', function(done) {
      config = {url: url};

      healthcheck(config, dependencies, function(error, data) {
        expect(data).to.have.property('url', url);
      });
      done();
    });

  }

};

describe('healthcheck', function() {

  describe('failure', function() {

    beforeEach(function(done) {
      config = helper.empty().config;
      job_callback = helper.empty().job_callback;
      dependencies = helper.empty().dependencies;
      dependencies.logger = helper.quietLogger();
      done();
    });

    describe('where request callback receives an error parameter it', function() {
      var failure = 'Error: ETIMEDOUT';

      beforeEach(function(done) {
        dependencies.request = function(options, callback) {
          callback(failure, null, null);
        };
        done();
      });

      sharedSpecs.healthcheckFailure(failure);
      sharedSpecs.healthcheckAnything();

    });

    describe('where request callback receives null response it', function() {
      var failure = 'Error: Unable to handle response';

      beforeEach(function(done) {
        dependencies.request = function(options, callback) {
          callback(null, null, null);
        };
        done();
      });

      sharedSpecs.healthcheckFailure(failure);
      sharedSpecs.healthcheckAnything();

    });

    describe('where request callback receives undefined response it', function() {
      var failure = 'Error: Unable to handle response';

      beforeEach(function(done) {
        dependencies.request = function(options, callback) {
          callback(null, undefined, null);
        };
        done();
      });

      sharedSpecs.healthcheckFailure(failure);
      sharedSpecs.healthcheckAnything();

    });

    describe('where request callback receives false response it', function() {
      var failure = 'Error: Unable to handle response';

      beforeEach(function(done) {
        dependencies.request = function(options, callback) {
          callback(null, false, null);
        };
        done();
      });

      sharedSpecs.healthcheckFailure(failure);
      sharedSpecs.healthcheckAnything();

    });

    describe('where request callback receives bad JSON body it', function() {
      var failure = 'Error: Unable to parse as JSON';

      beforeEach(function(done) {
        dependencies.request = function(options, callback) {
          callback(null, {statusCode: 200}, '');
        };
        done();
      });

      sharedSpecs.healthcheckFailure(failure);
      sharedSpecs.healthcheckAnything();

    });

    describe('where request callback receives a non-200 status it', function() {
      var failure = 'Error: Response status 504';

      beforeEach(function(done) {
        dependencies.request = function(options, callback) {
          callback(null, {statusCode: 504}, '');
        };
        done();
      });

      sharedSpecs.healthcheckFailure(failure);
      sharedSpecs.healthcheckAnything();

    });

  });

  describe('when healthy it', function() {

    beforeEach(function(done) {
      config = helper.empty().config;
      job_callback = helper.empty().job_callback;
      dependencies = helper.empty().dependencies;
      done();
    });

    beforeEach(function(done) {
      dependencies.request = function(options, callback) {
        callback(null, {statusCode: 200}, JSON.stringify({isHealthy: true, name: appName, hosts: 1}) );
      };
      done();
    });

    sharedSpecs.healthcheckAnything();

    it('is healthy', function(done) {
      healthcheck(config, dependencies, function(error, data) {
        expect(data).to.have.property('isHealthy', true);
      });
      done();
    });

    it('uses name in widgetTitle', function(done) {
      healthcheck(config, dependencies, function(error, data) {
        expect(data).to.have.property('widgetTitle', appName);
      });
      done();
    });

    it('provides the number of subscribed tenants', function(done) {
      healthcheck(config, dependencies, function(error, data) {
        expect(data).to.have.property('tenants', 1);
      });
      done();
    });

  });

});
