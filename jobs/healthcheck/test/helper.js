chai = require('chai');
assert = chai.assert;
expect = chai.expect;
should = chai.should();

exports.require = function(file) {
  return require('../' + file);
};

exports.complainingLogger = function() {
  return {
    name: 'complainingLogger',
    log: console.log,
    warn: console.log,
    error: function(message) {
      assert(null, 'Unexpected call of logger.error("' + message + '")');
    }
  };
};

exports.quietLogger = function() {
  return {
    name: 'quietLogger',
    log: function(message) {},
    warn: function(message) {},
    error: function(message) {}
  };
};

exports.empty = function() {
  return {
    config: {},
    job_callback: function() {},
    dependencies: {
      request: function() {},
      logger: exports.complainingLogger()
    }
  };
};
